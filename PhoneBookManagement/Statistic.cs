﻿public class Statistic
{
    public int TotalContact { get; set; }
    public int TotalFamilyContact { get; set; }
    public int TotalWorkContact { get; set; }
    public int TotalFriendContact { get; set; }
}
